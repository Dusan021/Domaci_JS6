var App = (function (axios, Mustache) {
	var genres = [];
	var movies = [];

	var $genres = document.getElementById('genres');
	var $movies = document.getElementById('movies');



	var genresTpl = document.getElementById('genres-template').innerHTML;
	var moviesTemplate = document.getElementById('movies-template').innerHTML;
	var descTemplate = document.getElementById('opis-filma').innerHTML;

	return {
		getCurrentGenre: function () {
		},
		getMoviesByGenre: function () {
		},
		start: function () {
			var self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				var data = response.data;
				genres = data.genres;
				movies = data.movies;

				self.createLinks();
			});
		},
		fetch: function (cb) {
			return axios.get('db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		 updateMovies: function () {
		 	// TODO: compile movies template;
		 	var nesto = location.hash.slice(2);

		 	var filtermovies = [];
		 	for(let i=0; i < movies.length; i++){
		 		for(let j=0; j < movies[i].genres.length; j++) {
		 			if  ( movies[i].genres[j] == nesto)
		 			filtermovies.push(movies[i])  ;

		 		}

		 	}

		 	$movies.innerHTML = Mustache.render(moviesTemplate, {
		 		movies : filtermovies });



/////////////////////////////////////////////////////////////////////

			 var opis = document.getElementById('opis-filma').innerHTML;

			function onWindowUnload() {
				openedWindow.removeEventListener('unload', onWindowUnload);
				openedWindow = null;
			}


			var element = document.getElementsByClassName('movie');
			var fMovie = null;
			//console.log(element);
			for(var i=0; i<element.length; i++){
			//	console.log(element[i]);
				 element[i].addEventListener('click',function(e){
					// console.log(e.currentTarget.id);
					 for(var g = 0; g < movies.length; g++){
						  //console.log(movies[g]);
							if(movies[g].id == e.currentTarget.id){
								fMovie = movies[g];
								break;


							};
					 }

					openedWindow = window.open(
						'', // URL ili prazan string za prazan prozor
						'movie_info', // Naziv prozora, ako se ne navede uvek otvara novi prozor
						'resizable=0,width=320,height=240'
					);//console.log(movies);
					openedWindow.document.title = 'Movie Info';
					openedWindow.document.body.innerHTML = Mustache.render(opis, {
						movies: fMovie

					});


				});
				//description-template === opis-filma


			};

			},






		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));

		},


	}
})(axios, Mustache);
