<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Movie DB</title>
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	<script type="text/javascript" src="jq/jquery-3.2.1.js"></script>
</head>

<body>

	<div class="container">
		<div class="login">
			<?php


				require("header.php");

			?>
		</div>
		<button id="dugme" type="button" name="button">genres</button>

			<ul id="genres"></ul>
			<div id="movies"></div>
			<div id="movie"></div>


			<script id="genres-template" type="x-tmpl-mustache">
				{{#genres}}
					<li id="link" class="link"><a href="#!{{.}}">{{.}}</a></li>
				{{/genres}}
			</script>

			<script id="movies-template" type="x-tmpl-mustache">
				{{#movies}}
					<div class="movie" id="{{id}}">
						<img src="{{posterUrl}}" alt="poster" />
						<h2>{{title}}</h2>
					</div>
				{{/movies}}
			</script>
			<script id="opis-filma" type="x-tmpl-mustache">
					{{#movies}}
						<div class="description" style="padding: 20px;">
							<h2>{{title}}</h2>
							<div class="alert alert-light" role="alert">
								<p><strong>Year:</strong> {{year}}</p>
								<p><strong>Runtime:</strong> {{runtime}}</p>
								<p><strong>Genres:</strong> {{genres}}</p>
								<p><strong>Director:</strong> {{director}}</p>
								<p><strong>Actors:</strong> {{actors}}</p>
							</div>
							<div class="alert alert-warning" role="alert">{{plot}}</div>
						</div>
					{{/movies}}
				</script>





			<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
			<script src="https://unpkg.com/mustache@2.3.0/mustache.min.js"></script>
			<script src="index.js"></script>
			<script>
				App.start();
			</script>
			<script type="text/javascript">
				$("#dugme").on('click', (function(){
					$('#genres').animate({marginLeft: 0},500);
					$('#genres').animate({fontSize: 21},360);




				}));
				$('ul').on('click', (function(){
					$('#movies').animate({marginRight: 5},500);

				}));



			</script>
	</div>







</body>

</html>
